package com.gs.restApi.serviceImpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gs.restApi.model.Customer;
import com.gs.restApi.repository.CustomerRepository;
import com.gs.restApi.serviceApi.CustomerService;

@Service
public class CustomerServiceImpl implements CustomerService{
	@Autowired
	private CustomerRepository customerRepository;

	@Override
	public Customer createCustomer(Customer customer) {
		return this.customerRepository.save(customer);
	}

	@Override
	public List<Customer> findAll() {
		List<Customer> customerList = this.customerRepository.findAll();
		return customerList;
	}

	@Override
	public Customer findById(String id) {
		Optional<Customer> optCustomer = this.customerRepository.findById(id);
		return optCustomer.get();
	}

	@Override
	public List<Customer> findByFirstName(String firstName) {
		return this.customerRepository.findByFirstName(firstName);
	}

	@Override
	public List<Customer> findByLastName(String lastName) {
		return this.customerRepository.findByLastName(lastName);
	}

	@Override
	public Customer updateCustomer(Customer customer) {
		Optional<Customer> customerOpt = this.customerRepository.findById(customer.getId());
		if(customerOpt.isPresent()) {
			return this.customerRepository.save(customer);
		}
		else {
			return null;
		}
	}

	@Override
	public void deleteById(String id) {
		this.customerRepository.deleteById(id);
	}

}
