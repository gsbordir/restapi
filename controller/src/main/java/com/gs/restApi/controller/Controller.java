package com.gs.restApi.controller;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.gs.category.client.CategoryClient;
import com.gs.category.requestDTO.CategoryRequestDTO;
import com.gs.restApi.controllerService.ObjectConverter;
import com.gs.restApi.dto.CustomerDTO;
import com.gs.restApi.model.Customer;
import com.gs.restApi.serviceImpl.CustomerServiceImpl;

@RestController
public class Controller{
	@Autowired
	private CustomerServiceImpl customerServiceImpl;
	@Autowired
	private ObjectConverter objectConverter;
	
	@Autowired
	private CategoryClient categoryClient;
	
	@PostMapping("/createCustomer")
	public Customer createCustomer(@RequestBody CustomerDTO customerDTO) {
		Customer customer = this.objectConverter.convertObject(customerDTO, Customer.class);
		return this.customerServiceImpl.createCustomer(customer);
	}
	
	@GetMapping("/findAllCustomer")
	public List<CustomerDTO> findAllCustomer()  throws JsonMappingException, JsonProcessingException{
		//run project as springboot app, springboot has embedded tomcat will run on 8080 port. in this example i change the port to 8081
		//to change the port of tomcat add "server.port=value" in application.properties
		// when tomcat started, to access this api you need to hit url "http://localhost:8081/getData"
		
		List<Customer> customerList = this.customerServiceImpl.findAll();
		ResponseEntity<List<CategoryRequestDTO>> categoryResponse = this.categoryClient.findAllCategory();
		System.out.println(categoryResponse);
		return this.objectConverter.convertObjectList(customerList, CustomerDTO.class);
	}
	
	@PostMapping("/findById")
	public Customer findById(@RequestBody CustomerDTO customerDTO) {
		return this.customerServiceImpl.findById(customerDTO.getId());
	}
	
	@PostMapping("/findByLastName")
	public Object findByLastName(@RequestBody CustomerDTO customerDTO){
		return this.customerServiceImpl.findByLastName(customerDTO.getLastName());
	}
	
	@PostMapping("/findByFirstName")
	public Object findByFirstName(@RequestBody CustomerDTO customerDTO){
		return this.customerServiceImpl.findByFirstName(customerDTO.getFirstName());
	}
	
	@PutMapping("/updateCustomer")
	public Customer updateCustomer(@RequestBody Object customerDTO) {
		Customer customer = this.objectConverter.convertObject(customerDTO, Customer.class);
		return this.customerServiceImpl.updateCustomer(customer);
	}
	
	@DeleteMapping("/deleteById")
	public void deleteCustomerById(@RequestBody CustomerDTO customerDTO) {
		this.customerServiceImpl.deleteById(customerDTO.getId());
	}
}
