package com.gs.restApi.controllerServiceImpl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gs.restApi.controllerService.ObjectConverter;

@Service
public class ObjectConverterImpl implements ObjectConverter{
	private ObjectMapper objectMapper = new ObjectMapper();

	@Override
	public <T> T convertObject(Object object, Class<T> targetClass) {
		return objectMapper.convertValue(object, targetClass);
	}

	@Override
	public <T> List<T> convertObjectList(Object object, Class<T> targetClass) throws JsonMappingException, JsonProcessingException {
		String json = this.objectMapper.writeValueAsString(object);
		JavaType type = objectMapper.getTypeFactory().constructCollectionType(List.class, targetClass);
		return this.objectMapper.readValue(json, type);
	}

}
