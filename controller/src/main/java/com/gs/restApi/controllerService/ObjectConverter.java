package com.gs.restApi.controllerService;

import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

public interface ObjectConverter {
	public <T> T convertObject(Object object, Class<T> targetClass);
	public <T> List<T> convertObjectList(Object object, Class<T> targetClass) throws JsonMappingException, JsonProcessingException;
}
