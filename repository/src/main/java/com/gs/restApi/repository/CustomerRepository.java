package com.gs.restApi.repository;

import java.util.List;
import java.util.Optional;

import com.gs.restApi.model.Customer;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

public interface CustomerRepository extends MongoRepository<Customer, String> {
	public List<Customer> findAll();
	public Optional<Customer> findById(String id);
	public List<Customer> findByFirstName(String firstName);
	public List<Customer> findByLastName(String lastName);
	public void deleteById(String id);
}
