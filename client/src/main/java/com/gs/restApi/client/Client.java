package com.gs.restApi.client;

import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.gs.restApi.dto.CustomerDTO;

@SpringBootApplication
public class Client implements ApplicationRunner{
	private RestTemplate restTemplate = new RestTemplate();
	
	@Value("${restApi.baseUrl}")
	private String baseUrl;
	
	public ResponseEntity<List<CustomerDTO>> findAllCustomer() {
		String findAllCustomerUrl = new StringBuilder(this.baseUrl).append("/findAllCustomer").toString();
		return restTemplate.exchange(findAllCustomerUrl, HttpMethod.GET, null, new ParameterizedTypeReference<List<CustomerDTO>>() {});
	}
	
	public static void main(String[] args) {
		SpringApplication.run(Client.class, args);
	}

	@Override
	public void run(ApplicationArguments args) throws Exception {
		System.out.println(this.findAllCustomer());
	}
}
