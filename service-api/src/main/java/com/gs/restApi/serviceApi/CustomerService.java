package com.gs.restApi.serviceApi;

import java.util.List;

import com.gs.restApi.model.Customer;

public interface CustomerService {
	public Customer createCustomer(Customer customer);
	public List<Customer> findAll();
	public Customer findById(String id);
	public List<Customer> findByFirstName(String firstName);
	public List<Customer> findByLastName(String lastName);
	public Customer updateCustomer(Customer customer);
	public void deleteById(String id);
}
